'use strict'

var angular = require('angular');
require('restangular');
require('@uirouter/angularjs');

var routerConfig = require('./config/router/router.config');
var restangularConfig = require('./config/restangular/restangular.config');


module.exports = angular
    .module('myApp', [
        'restangular',
        'ui.router'
    ])
    .config(routerConfig)
    .config(restangularConfig)