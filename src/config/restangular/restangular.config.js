'use strict'

module.exports = configRestangular

configRestangular.$inject = ['RestangularProvider']

function configRestangular (RestangularProvider) {
  var newBaseUrl = ''
  if (window.location.hostname === 'localhost') {
    newBaseUrl = 'http://localhost:9000/api/v1/'
  } else {
    var deployedAt = window.location.href.substring(0, window.location.href)
    newBaseUrl = deployedAt + '/api/rest/register'
  }
  RestangularProvider.setBaseUrl(newBaseUrl)
}
