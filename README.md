# Sistema para cadastro de roupas

* Sistema criado para fins didático

# Tecnologias utilizadas.

* Webpack na versão 4
* Angular JS na versão 1.4.14

# Como executar o projeto

* git clone https://gitlab.com/amandasomariva/cadastro-roupa.git
* cd cadastro-roupa
* npm install
* npm start
* Vá ao navegador e entra no *http://localhost:8080*